<?php

    namespace WorkflowManager\Configuration;
    
    abstract class Env
    {
        static $SAL_ENDPOINT = null;
        static $WFS_ZEROMQ_ENDPOINT = null;
        static $WFS_ZEROMQ_SOURCE_ENDPOINT = null;
        static $WFS_IP = null;
        static $CTRL_IP = null;
        static $WFS_NOTIFICATION_ENDPOINT = null;
        static $SPECIAL_PATH_WF_FOLDER_PLACEHOLDER = "<WF-UUID>";
        static $SPECIAL_PATH_WF_UUID_PLACEHOLDER = "<WF-UUID-NO-PATH>";
        static $WORKFLOW_BASE_FOLDER = null;
    };
