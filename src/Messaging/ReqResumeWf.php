<?php

    namespace WorkflowManager\Messaging;
    
    class ReqResumeWf extends AbstractReq {
        const REQUEST = "resume";
    
        public $wfUuid;
        public $data;
    
        function __construct($wfUuid) {
            parent::__construct(self::REQUEST);
            $this->wfUuid = $wfUuid;
            $this->message = "Resume workflow " . $wfUuid;
            $this->data = new \stdClass();
        }
    }
