<?php

    namespace WorkflowManager\Messaging;
    
    abstract class AbstractResp {
    
        public $source;
        public $destination;
        public $timestamp;
        public $requestUuid;
        public $response;
        public $notificationEndpoint;
        public $message;
    
        function __construct($jsonedObject) {
            $this->response = $jsonedObject->response;
            $this->source = $jsonedObject->source;
            $this->destination = $jsonedObject->destination;
            if (isset($jsonedObject->requestUuid))
                $this->requestUuid = $jsonedObject->requestUuid;
            if (isset($jsonedObject->message))
                $this->message = $jsonedObject->message;
        }
    
    
    }
