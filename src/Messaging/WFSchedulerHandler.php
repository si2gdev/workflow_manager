<?php

    namespace WorkflowManager\Messaging;
    
    use WorkflowManager\LogUtil;
    use ZMQ;
    use ZMQContext;
    use ZMQSocket;
    
    class WFSchedulerHandler {
        public static $_rcvTimeout = 60000;
        public static $_sndTimeout = 60000;
        private $connectionString;
        /** @var  LogUtil */
        private $logUtil;
        /** @var  ZMQSocket */
        private $zeroMQClient;
    
        function __construct($connectionString) {
            $this->connectionString = $connectionString;
            $this->logUtil = new LogUtil();
        }
    
        protected function connectToZeroMQ() {
            // initialize ZMQContext
            $context = new ZMQContext();
            $this->zeroMQClient = new ZMQSocket($context, ZMQ::SOCKET_REQ);
            $this->zeroMQClient->setSockOpt(ZMQ::SOCKOPT_RCVTIMEO, self::$_rcvTimeout);
            $this->zeroMQClient->setSockOpt(ZMQ::SOCKOPT_SNDTIMEO, self::$_sndTimeout);
    
            // then connect to WFS
            $this->logUtil->getLogger()->debug("Connecting to " . $this->connectionString . " ...");
            $this->zeroMQClient->connect($this->connectionString);
            $this->logUtil->getLogger()->debug("Connected to " . $this->connectionString);
        }
    
        /**
         * @param AbstractReq $msg
         */
        protected function sendMessage(AbstractReq $msg) {
            $this->logUtil->getLogger()->debug("Sending message to the channel...");
            $this->logUtil->getLogger()->debug(json_encode($msg));
            $this->zeroMQClient->send(json_encode($msg));
            $this->logUtil->getLogger()->debug("Sent message to the channel.");
        }
    
        /**
         * @param AbstractReq $req
         *
         * @return mixed
         * @throws WFSMessagingException
         */
        protected function sendRequest(AbstractReq $req) {
            $this->connectToZeroMQ();
    
            $this->sendMessage($req);
            // wait for the ack
            $replyString = $this->receiveMessageString();
            if (!$replyString) {
                // timeout: raise exception?
                throw new WFSTimeoutException($req->source, $req->destination, self::$_rcvTimeout);
            } else {
                $reply = json_decode($replyString);
                if ($reply == null)
                    throw new WFSMessagingException($req->source, $req->destination);
                return $reply;
            }
        }
    
        /**
         * @return string
         */
        protected function receiveMessageString() {
            return $this->zeroMQClient->recv();
        }
    
        /**
         * @param $wfUUID
         * @param $wfDef
         *
         * @return RespStartWf
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         */
        public function startWorkflow($wfUUID, $wfDef) {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqStartWf($wfUUID, $wfDef));
            // create the proper Response object from the JSON response
            return new RespStartWf($replyJsonObj);
        }
    
        /**
         * @param $wfUUID
         * @param $wfDef
         *
         * @return RespRestartWf
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         */
        public function restartWorkflow($wfUUID, $wfDef) {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqRestartWf($wfUUID, $wfDef));
            // create the proper Response object from the JSON response
            return new RespRestartWf($replyJsonObj);
        }
    
        /**
         * @param $wfUUID
         *
         * @return mixed
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         */
        public function pauseWorkflow($wfUUID) {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqPauseWf($wfUUID));
            // create the proper Response object from the JSON response
            $reply = new RespPauseWf($replyJsonObj);
    
            // TODO: something went wrong with the request, what to do with the ack??
            return $reply->acknowledge;
        }
    
        /**
         * @param $wfUUID
         *
         * @return mixed
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         */
        public function resumeWorkflow($wfUUID) {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqResumeWf($wfUUID));
            // create the proper Response object from the JSON response
            $reply = new RespResumeWf($replyJsonObj);
    
            // TODO: something went wrong with the request, what to do with the ack??
            return $reply->acknowledge;
        }
    
        /**
         * @param $wfUUID
         *
         * @return mixed
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         */
        public function stopWorkflow($wfUUID) {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqStopWf($wfUUID));
            // create the proper Response object from the JSON response
            $reply = new RespStopWf($replyJsonObj);
    
            // TODO: something went wrong with the request, what to do with the ack??
            return $reply->acknowledge;
        }
    
        /**
         * @param $wfUUID
         *
         * @return mixed
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         * @throws WorkflowNotFoundException
         */
        public function askWorkflowStatus($wfUUID) {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqStatusWf($wfUUID));
            // create the proper Response object from the JSON response
            $reply = new RespStatusWf($replyJsonObj);
    
            // TODO: something went wrong with the request, what to do with the ack??
            return $reply->acknowledge;
        }
    
        /**
         * @return \WorkflowManager\Workflow\SchedulerStatus|null
         * @throws WFSMessagingException
         * @throws WFSTimeoutException
         */
        public function getServiceStatus() {
            // send the request through socket and parse JSON response
            $replyJsonObj = $this->sendRequest(new ReqService());
    
            // create the proper Response object from the JSON response
            if ($replyJsonObj->acknowledge || $replyJsonObj->acknowledge == "true") {
                $reply = new RespService($replyJsonObj);
    
                return $reply->data;
            } else {
                throw new WFSMessagingException($replyJsonObj->destination, $replyJsonObj->source);
            }
        }
    
    }
