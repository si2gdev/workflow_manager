<?php

    namespace WorkflowManager\Messaging;
    
    class ReqStartWf extends AbstractReq {
        const REQUEST = "start";
    
        public $wfUuid;
        public $data;
    
        function __construct($wfUuid, $definition) {
            parent::__construct(self::REQUEST);
            $this->wfUuid = $wfUuid;
            $this->message = "Start workflow " . $wfUuid;
            $this->data = $definition;
        }
    }
