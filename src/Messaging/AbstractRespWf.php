<?php

    namespace WorkflowManager\Messaging;
    
    abstract class AbstractRespWf extends AbstractResp {
        public $wfUuid;
        public $acknowledge;
        public $status;
        public $data;
    
        /**
         * AbstractRespWf constructor.
         *
         * @param $jsonedObject
         *
         * @throws WorkflowNotFoundException
         */
        function __construct($jsonedObject) {
            parent::__construct($jsonedObject);
            $this->wfUuid = $jsonedObject->wfUuid;
            $this->acknowledge = $jsonedObject->acknowledge;
            $this->status = $jsonedObject->status;
            $this->data = isset($jsonedObject->data) ? $jsonedObject->data : null;
    
            // TODO: validate all the error statuses
            if ($this->status == 404) {
                // WF not found
                throw new WorkflowNotFoundException($this->source, $this->destination, $this->wfUuid);
            }
        }
    
    
    }
