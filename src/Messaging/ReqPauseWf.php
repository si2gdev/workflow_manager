<?php

    namespace WorkflowManager\Messaging;
    
    class ReqPauseWf extends AbstractReq {
        const REQUEST = "pause";
    
        public $wfUuid;
        public $data;
    
        function __construct($wfUuid) {
            parent::__construct(self::REQUEST);
            $this->wfUuid = $wfUuid;
            $this->message = "Pause workflow " . $wfUuid;
            $this->data = new \stdClass();
        }
    }
