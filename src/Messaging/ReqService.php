<?php

    namespace WorkflowManager\Messaging;
    
    class ReqService extends AbstractReq{
        const REQUEST = "service";
    
        public $data;
    
        /**
         * ReqService constructor.
         */
        public function __construct() {
            parent::__construct(self::REQUEST);
        }
    
    }
