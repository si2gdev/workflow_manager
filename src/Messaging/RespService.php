<?php

    namespace WorkflowManager\Messaging;
    
    use WorkflowManager\Workflow\SchedulerStatus;
    
    class RespService extends AbstractResp {
    
        /**
         * @var SchedulerStatus
         */
        public $data;
    
        /**
         * RespService constructor.
         *
         * @param $jsonObject
         */
        public function __construct($jsonObject) {
            parent::__construct($jsonObject);
            $this->data = isset($jsonObject->data) ? new SchedulerStatus($jsonObject->data) : null;
        }
    }
