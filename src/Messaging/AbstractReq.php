<?php

    namespace WorkflowManager\Messaging;
    
    use WorkflowManager\Configuration\Env;
    use DateTime;
    
    abstract class AbstractReq {
        public $source;
        public $destination;
        public $timestamp;
        public $requestUuid;
        public $request;
        public $notificationEndpoint;
        public $message;
    
        function __construct($request) {
            $this->request = $request;
            $this->requestUuid = \Ramsey\Uuid\Uuid::uuid4()->toString();
            $this->source = Env::$WFS_ZEROMQ_SOURCE_ENDPOINT;
            $this->destination = Env::$WFS_ZEROMQ_ENDPOINT;
            $this->timestamp = (new DateTime())->format('c');
            $this->notificationEndpoint = [Env::$WFS_NOTIFICATION_ENDPOINT];
        }
    
    }
