<?php

    namespace WorkflowManager\Messaging;
    
    class ReqStopWf extends AbstractReq {
        const REQUEST = "stop";
    
        public $wfUuid;
        public $data;
    
        function __construct($wfUuid) {
            parent::__construct(self::REQUEST);
            $this->wfUuid = $wfUuid;
            $this->message = "Stop workflow " . $wfUuid;
        }
    }
