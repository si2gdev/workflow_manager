<?php

    namespace WorkflowManager\Messaging;
    
    
    class WorkflowNotFoundException extends WFSMessagingException {
        public $workflowUuid;
    
        /**
         * WorkflowNotFoundException constructor.
         *
         * @param string $sourceEndpoint
         * @param int    $destinationEndpoint
         * @param        $workflowUuid
         */
        function __construct($sourceEndpoint, $destinationEndpoint, $workflowUuid) {
            WFSMessagingException::__construct($sourceEndpoint, $destinationEndpoint);
            $this->workflowUuid = $workflowUuid;
        }
    
    }
