<?php

    namespace WorkflowManager\Messaging;
    
    
    class WFSMessagingException extends \Exception {
    
        public $sourceEndpoint;
        public $destinationEndpoint;
    
        function __construct($sourceEndpoint, $destinationEndpoint) {
            $this->sourceEndpoint = $sourceEndpoint;
            $this->destinationEndpoint = $destinationEndpoint;
        }
    
    
    }
