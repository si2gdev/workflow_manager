<?php

    namespace WorkflowManager\Messaging;
    
    class ReqStatusWf extends AbstractReq {
        const REQUEST = "status";
    
        public $wfUuid;
        public $data;
    
        function __construct($wfUuid) {
            parent::__construct(self::REQUEST);
            $this->wfUuid = $wfUuid;
            $this->message = "Status of workflow " . $wfUuid;
            $this->data = new \stdClass();
        }
    }
