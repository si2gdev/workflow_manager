<?php

    namespace WorkflowManager\Messaging;
    
    class ReqRestartWf extends AbstractReq {
        const REQUEST = "restart";
    
        public $wfUuid;
        public $data;
    
        function __construct($wfUuid, $definition) {
            parent::__construct(self::REQUEST);
            $this->wfUuid = $wfUuid;
            $this->message = "Restart workflow " . $wfUuid;
            $this->data = $definition;
        }
    }
