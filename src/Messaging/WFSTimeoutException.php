<?php

    namespace WorkflowManager\Messaging;
    
    
    class WFSTimeoutException extends WFSMessagingException {
        public $timeout;
    
        function __construct($sourceEndpoint, $destinationEndpoint, $timeout) {
            parent::__construct($sourceEndpoint, $destinationEndpoint);
            $this->timeout = $timeout;
        }
    
    }
