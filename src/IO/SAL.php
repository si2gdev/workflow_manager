<?php

    namespace WorkflowManager\IO;
    
    use WorkflowManager\Configuration\Env;
    use WorkflowManager\LogUtil;
    use WorkflowManager\StringUtil;
    use GuzzleHttp\Client;
    
    class SAL {
        const COMMUNICATION_TIME = 40.0; // number of seconds before a timeout interrupt the connection (on strange Windows installation the request does not fire up until 21 seconds passed by, so we need to move this timeout to an high value to let everything working, although slowly.)
    
        public static function hasExtension($path, $ext) {
            if (!StringUtil::startsWith($ext, ".")) {
                // ensure the extension starts with a dot
                $ext = "." . $ext;
            }
            return StringUtil::endsWith($path, $ext);
        }
    
        public static function removeExtension($path) {
            return preg_replace('/\\.[^.\\s]*$/', '', $path);
        }
    
        public static function stripFileProtocol($path) {
            if (StringUtil::startsWith($path, "file://")) {
                $path = substr($path, 7);
            }
            return $path;
        }
    
        public static function translatePath($path, $destinationIp, $sourceIp) {
            // Instantiate a logger to trace the SAL request execution
            $logger = (new LogUtil(null, null, "SAL"))->getLogger();
            try {
                $client = new Client([
                    // Base URI is used with relative requests
                    'base_uri' => Env::$SAL_ENDPOINT . "/",
                    // You can set any number of default request options.
                    'timeout' => self::COMMUNICATION_TIME,
                ]);
    
                $requestUrl = Env::$SAL_ENDPOINT . "/translated-path/?format=json";
    
                $logger->debug("Trying to translate single path with a GET to the following URL: " . $requestUrl);
    
                $response = $client->request("GET",
                    $requestUrl,
                    ["query" => [
                        "path" => $path,
                        "source-ip" => $sourceIp,
                        "destination-ip" => $destinationIp
                    ]]);
    
                return json_decode($response->getBody()->getContents());
            } catch (\Exception $ex) {
                // wrap the Guzzle exception into a SALCommunicationException for logging and better management
                $logger->error("Unable to translate single path from source (" . $sourceIp . ") to destination (" . $destinationIp . "). The stack track is reported in the next lines.");
                $logger->error($ex->getMessage());
                $logger->error($ex->getTraceAsString());
    
                throw new SALCommunicationException();
            }
        }
    
        public static function translatePaths($paths, $destinationIp, $sourceIp, $fileProtocolPreference = false) {
            // Instantiate a logger to trace the SAL request execution
            $logger = (new LogUtil(null, null, "SAL"))->getLogger();
    
            $bulk = new BulkPathsTranslation();
            $bulk->sourceIp = $sourceIp;
            $bulk->destinationIp = $destinationIp;
            $bulk->fileProtocolPreference = $fileProtocolPreference;
            $bulk->paths = $paths;
    
            try {
                $client = new Client([
                    // Base URI is used with relative requests
                    'base_uri' => Env::$SAL_ENDPOINT . "/",
                    // You can set any number of default request options.
                    'timeout' => self::COMMUNICATION_TIME,
                ]);
    
                $requestUrl = Env::$SAL_ENDPOINT . "/translated-path/?format=json";
    
                $logger->debug("Trying to translate bulk paths with a POST to the following URL: " . $requestUrl);
    
                $response = $client->request("POST",
                    $requestUrl,
                    ["json" => (array)$bulk]);
    
                $reply = json_decode($response->getBody()->getContents());
    
                $translations = [];
                foreach ($reply->data->translations as $translation) {
                    if ($translation->untranslatable) {
                        $translations[$translation->originalPath] = $translation->originalPath;
                    } else {
                        $translations[$translation->originalPath] = $translation->translatedPath;
                    }
                }
                return $translations;
            } catch (\Exception $ex) {
                // wrap the Guzzle exception into a SALCommunicationException for logging and better management
                $logger->error("Unable to translate bulk paths from source (" . $sourceIp . ") to destination (" . $destinationIp . "). The stack track is reported in the next lines.");
                $logger->error($ex->getMessage());
                $logger->error($ex->getTraceAsString());
    
                throw new SALCommunicationException();
            }
        }
    
        /**
         * Valid paths start with file://, http://, https://
         *
         * @param $path
         *
         * @return bool
         */
        public static function isPath($path) {
            return StringUtil::startsWithAlternatives($path, ["file://", "http://", "https://"]);
        }
    
        /**
         * If the given parameter is an URI, it converts backslashes to forward slashes.
         *
         * @param $path
         *
         * @return mixed
         */
        public static function sanitizeURI($path) {
            // replace the backward slash with canonical forward slash
            if (SAL::isPath($path))
                return str_replace('\\', '/', $path);
            else
                return $path;
        }
    
        public static function isWorkingFile($path) {
            // remove file:// and http:// prefixes,
            $cleanedPath = str_replace("file://", "", $path);
            $cleanedPath = str_replace("http://", "", $cleanedPath);
            //to check if there are any other slash
            return strpos($cleanedPath, "/") === FALSE;
        }
    
        public static function canonicalPath($path) {
            $realpath = realpath($path);
            if ($realpath)
                $path = $realpath;
    
            if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') {
                // on Windows systems, replace the backward slash with canonical forward slash
                $path = str_replace('\\', '/', $path);
            }
            return $path;
        }
    }
