<?php

    namespace WorkflowManager\IO;
    
    
    class BulkPathsTranslation {
    
        /**
         * @var string[]
         */
        public $paths;
        /**
         * @var string
         */
        public $sourceIp;
        /**
         * @var string
         */
        public $destinationIp;
    
        /**
         * @var boolean
         */
        public $fileProtocolPreference;
    }
