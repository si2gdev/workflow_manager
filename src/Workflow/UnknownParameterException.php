<?php

    namespace WorkflowManager\Workflow;
    
    
    class UnknownParameterException extends \Exception {
        public $parameter;
    
        function __construct($parameter) {
            parent::__construct("Unknown parameter " . $parameter);
            $this->parameter = $parameter;
        }
    
    }
