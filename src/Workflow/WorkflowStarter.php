<?php

    namespace WorkflowManager\Workflow;
    
    use WorkflowManager\Configuration\Env;
    use WorkflowManager\IO\SAL;
    use WorkflowManager\Messaging\WFSchedulerHandler;
    use WorkflowManager\Messaging\RespStartWf;
    
    abstract class WorkflowStarter {
        /**
         * Send the start/restart command for the WF to the WFS. If the WFS replies that the WF has been enqueued, put its
         * status (and the status of each of its step) to STATUS_NEW.
         *
         * @param                    $zmqEndpoint
         * @param WorkflowDefinition $workflowDefinition
         * @param string             $wfUuid
         *
         * @return RespStartWf
         */
        public static function sendStart(WorkflowDefinition $workflowDefinition, $wfUuid) {
            // use SAL to translate path from local to Remotely readable.
            self::translatePaths($workflowDefinition);
    
            // ask the WFScheduler to start the workflow
            $zmqEndpoint = Env::$WFS_ZEROMQ_ENDPOINT;
            $wfsHandler = new WFSchedulerHandler($zmqEndpoint);
            $reply = $wfsHandler->startWorkflow($wfUuid, $workflowDefinition);
    
            return $reply;
        }
    
        /**
         * Translate input paths in order to allow the remote WFS to read them even if it's in a remote machine: if the
         * path is a working path (relative path) do not translate it. Output paths are considered as simple file access
         * through WFS local drives.
         *
         * @param WorkflowDefinition $def
         */
        private static function translatePaths(WorkflowDefinition $def) {
    		//MIRCO_20170513
    		return;
            // extract all paths of the WorkflowDefinition
            $paths = [];
            foreach ($def->steps as $s) {
                foreach ($s->inputs as $i) {
                    if (is_array($i->path)) {
                        $arrayOfPaths = $i->path;
                    } else {
                        $arrayOfPaths = [$i->path];
                    }
                    foreach ($arrayOfPaths as $p) {
                        if (SAL::isPath($p) // translate only paths (plain strings are sort of parameters that MUST be used in the input array)
                            && !SAL::isWorkingFile($p) // relative paths should not been translated
                        )
                            // absolute path, translate it
                            $paths[] = $p;
                    }
                }
            }
    
            // then use SAL to obtain paths readable by the WFS (using file:// protocol if possible)
            $translations = SAL::translatePaths($paths, Env::$WFS_IP, Env::$CTRL_IP, true);
    
            // finally, re-map all the translated paths
            foreach ($def->steps as $s) {
                foreach ($s->inputs as $i) {
                    if (is_array($i->path)) {
                        // multiple I/O per key
                        for ($k = 0; $k < count($i->path); $k++) {
                            if (SAL::isPath($i->path[$k]) // translate only paths (plain strings are sort of parameters that MUST be used in the input array)
                                && !SAL::isWorkingFile($i->path[$k]) // relative paths should not been translated
                            )
                                // absolute path, translate it
                                $i->path[$k] = $translations[$i->path[$k]];
                        }
                    } else {
                        // single I/O per key
                        if (SAL::isPath($i->path) // translate only paths (plain strings are sort of parameters that MUST be used in the input array)
                            && !SAL::isWorkingFile($i->path) // relative paths should not been translated
                        )
                            // absolute path, translate it
                            $i->path = $translations[$i->path];
                    }
                }
            }
        }
    }
