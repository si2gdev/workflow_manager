<?php

    namespace WorkflowManager\Workflow\Status;

    /**
     * Serie of defines for workflow status states.
     * 
     * @author Fieldtronics
     *
     */
    abstract class WorkflowStatus
    {
        const UNKNOWN = 0;
        const NEW = 1;
        const PENDING_START = 2;
        const PENDING_RESUME = 3;
        const PREFETCHING = 5;
        const EXECUTING = 6;
        const COMPLETING = 7;
        
        const PENDING_PAUSE = 9;
        const PENDING_STOP = 10;
        const PENDING_ABORT = 11;
        
        const PAUSED = 16;
        const COMPLETED = 32;
        const STOPPED = 33;
        const ABORTED = 34;
        const ERROR = 35;
        
        private static $codec = array(
            'NEW'           => self::NEW,
            'PENDING_START' => self::PENDING_START,
            'PREFETCHING'   => self::PREFETCHING,
            'EXECUTING'     => self::EXECUTING,
            'COMPLETING'    => self::COMPLETING,
            'PENDING_PAUSE' => self::PENDING_PAUSE,
            'PAUSED'        => self::PAUSED,
            'PENDING_STOP'  => self::PENDING_STOP,
            'PENDING_ABORT' => self::PENDING_ABORT,
            'COMPLETED'     => self::COMPLETED,
            'STOPPED'       => self::STOPPED,
            'ERROR'         => self::ERROR,
            'ABORTED'       => self::ABORTED
        );
        
        /**
         * Convert string status name to its corresponding numeric code.
         * 
         * @param string $str_code : status code name, most of times from json.
         * @return int : numeric code, false o unknown code.
         */
        public static function parseStatus($str_code)
        {
            return self::$codec[$str_code] ?? self::UNKNOWN;
        }
        
        /**
         * Parse datetime string to DateTime object.
         *
         * @param string $str_dt : string representation of datetime (most of times from json).
         * @return \DateTime|null : new DateTime object or null if unparsable.
         */
        public static function parseDateTime($str_dt)
        {
            // first try to parse with microseconds and timezone
            $parsed = \DateTime::createFromFormat("Y-m-d\TH:i:s.uO", $str_dt);
            if ($parsed == null) {
                // unable to parse microseconds: try to parse it without them (but still with timezone)
                $parsed = \DateTime::createFromFormat("Y-m-d\TH:i:sO", $str_dt);
            }
            if ($parsed == null) {
                // unable to parse date with timezone, try to parse it without timezone
                $parsed = \DateTime::createFromFormat("Y-m-d\TH:i:s", $str_dt);
            }
            
            if( !$parsed )
                $parsed = null;
            
            return $parsed;
        }
    };
