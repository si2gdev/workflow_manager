<?php

    namespace WorkflowManager\Workflow\Status;
                
    class WFStatusRequestStep
    {
        /** @var int : step's block number. */
        public $block;
        /** @var int : step's index. */
        public $step;
        /** @var string : task's name. */
        public $task;
        /** @var int : workflow's step's status. Use WorkflowStatus constants. */
        public $status;
        /** @var int : progress of step. */
        public $progress;
        /** @var \DateTime : date and time of begin. */
        public $startTime;
        /** @var \DateTime : date and time of ending. */
        public $endTime;
        /** @var \DateTime : date and time of last update. */
        public $lastUpdate;
        /** @var int : return error code of step (0 is ok). */
        public $returnCode;
        /** @var string : message for step. */
        public $message;
        
        public function __construct()
        {
            $this->block = 0;
            $this->step = 0;
            $this->task = null;
            $this->status = WorkflowStatus::UNKNOWN;
            $this->progress = 0;
            $this->startTime = null;
            $this->endTime = null;
            $this->lastUpdate = null;
            $this->returnCode = 0;
            $this->message = null;
        }
        
        /**
         * Returns start_time in ISO format, or null if it's null.
         *
         * @return string|null
         */
        public function getStartTime()
        {
            if( !is_null($this->startTime) )
                return $this->startTime->format(DATE_ATOM);
            return null;
        }
        
        /**
         * Returns end_time in ISO format, or null if it's null.
         *
         * @return string|null
         */
        public function getEndTime()
        {
            if( !is_null($this->endTime) )
                return $this->endTime->format(DATE_ATOM);
            return null;
        }
        
        /**
         * Returns last_update in ISO format, or null if it's null.
         *
         * @return string|null
         */
        public function getLastUpdateTime()
        {
            if( !is_null($this->lastUpdate) )
                return $this->lastUpdate->format(DATE_ATOM);
            return null;
        }
        
        /**
         * Create, populate and returns a new WFStatusRequestStatus from scheduler's info array.
         * 
         * @param array $arr : scheduler's status info array.
         * @return WFStatusRequestStep : new WFStatusRequestStep object.
         */
        public static function parseArray($arr)
        {
            $newObj = new WFStatusRequestStep();
            $newObj->block = $arr['block'] ?? 0;
            $newObj->step = $arr['step'] ?? 0;
            $newObj->task = $arr['task'] ?? null;
            $newObj->status = WorkflowStatus::parseStatus($arr['status'] ?? null);
            $newObj->progress = $arr['progress'] ?? 0;
            $newObj->startTime = WorkflowStatus::parseDateTime($arr['startTime'] ?? null);
            $newObj->endTime = WorkflowStatus::parseDateTime($arr['endTime'] ?? null);
            $newObj->lastUpdate = WorkflowStatus::parseDateTime($arr['lastUpdate'] ?? null);
            $newObj->returnCode = $arr['returnCode'] ?? 0;
            $newObj->message = $arr['message'] ?? null;
            
            return $newObj;
        }
    };
