<?php

    namespace WorkflowManager\Workflow\Status;
    
    class WFStatusRequest
    {
        /** @var int : workflow status. */
        public $status;
        /** @var \DateTime : date and time of start. */
        public $startTime;
        /** @var \DateTime : date and time of end. */
        public $endTime;
        /** @var \DateTime : date and time of last update. */
        public $lastUpdate;
        /** @var string : message from scheduler. */
        public $message;
        /** @var WFStatusRequestWorkflow : specific workflow info. */
        public $workflow;
        
        public function __construct()
        {
            $this->status = WorkflowStatus::UNKNOWN;
            $this->startTime = null;
            $this->endTime = null;
            $this->lastUpdate = null;
            $this->message = null;
            $this->workflow = null;
        }
        
        /**
         * Returns start_time in ISO format, or null if it's null.
         *
         * @return string|null
         */
        public function getStartTime()
        {
            if( !is_null($this->startTime) )
                return $this->startTime->format(DATE_ATOM);
            return null;
        }
        
        /**
         * Returns end_time in ISO format, or null if it's null.
         *
         * @return string|null
         */
        public function getEndTime()
        {
            if( !is_null($this->endTime) )
                return $this->endTime->format(DATE_ATOM);
            return null;
        }
        
        /**
         * Returns last_update in ISO format, or null if it's null.
         *
         * @return string|null
         */
        public function getLastUpdate()
        {
            if( !is_null($this->lastUpdate) )
                return $this->lastUpdate->format(DATE_ATOM);
            return null;
        }
        
        /**
         * Create, populate and return a new object representing the request received from
         * the scheduler.
         *
         * @param string $json : request body from scheduler status.
         * @return WFStatusRequest|null : new object, or null is json parsing fails.
         */
        public static function parseJson($json)
        {
            $arr = json_decode($json, true);
            if($arr === false)
                return null;
                
            $newObj = new WFStatusRequest();
            $newObj->status = WorkflowStatus::parseStatus($arr['status'] ?? null);
            $newObj->startTime = WorkflowStatus::parseDateTime($arr['startTime'] ?? null);
            $newObj->endTime = WorkflowStatus::parseDateTime($arr['endTime'] ?? null);
            $newObj->lastUpdate = WorkflowStatus::parseDateTime($arr['lastUpdate'] ?? null);
            $newObj->message = $arr['message'] ?? null;
            $newObj->workflow = WFStatusRequestWorkflow::parseArray($arr['workflow'] ?? null);
            
            return $newObj;
        }
    };
