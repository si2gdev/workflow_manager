<?php

    namespace WorkflowManager\Workflow\Status;
    
    class WFStatusResponse
    {
        const API_VERSION = 1;
        const CONTENT_TYPE = 'application/json';
        
        /** @var string : api version. */
        protected $apiVersion;
        /** @var string : content type. */
        protected $contentType;
        /** @var int : http response code. */
        public $httpCode;
        /** @var string : error message string. */
        public $errorMsg;
        /** @var string : error code. */
        public $errorCode;
        /** @var string : details message back. */
        public $message;
        /** @var bool : acknowledge flag. */
        public $acknowledge;
        /** @var string : workflow unique identifier. */
        public $wfUuid;
        
        public function __construct($wf_uuid=null, $ack=true)
        {
            $this->apiVersion = self::API_VERSION;
            $this->contentType = self::CONTENT_TYPE;
            
            $this->httpCode = 200;
            $this->errorMsg = null;
            $this->errorCode = null;
            $this->message = null;
            $this->wfUuid = $wf_uuid;
            $this->acknowledge = $ack;
        }
        
        public function getApiVersion() { $this->apiVersion; }
        public function getContentType() { $this->contentType; }
        
        /**
         * Return json representation of member variables of this class.
         * 
         * @return string : json.
         */
        public function toJson()
        {
            $struct = array(
                'apiVersion' => $this->apiVersion,
                'httpCode' => $this->httpCode,
                'errorMsg' => $this->errorMsg,
                'errorCode' => $this->errorCode,
                'contentType' => $this->contentType,
                'message' => $this->message,
                'data' => array(
                    'wfUuid' => $this->wfUuid,
                    'acknowledge' => $this->acknowledge
                )
            );
            
            // in case of any error just clear data
            if( !empty($this->errorMsg) )
                $struct['data'] = array();
            
            return json_encode($struct);
        }
    };
