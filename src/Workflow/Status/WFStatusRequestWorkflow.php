<?php

    namespace WorkflowManager\Workflow\Status;
    
    class WFStatusRequestWorkflow
    {
        /** @var string : name of the workflow. */
        public $name;
        /** @var string : auth code for access validation. */
        public $auth;
        /** @var WFStatusRequestStep[] : array of steps in workflow. */
        public $steps;
        
        public function __construct()
        {
            $this->name = null;
            $this->auth = null;
            $this->steps = array();
        }
        
        /**
         * Create, populate and return a new WFStatusRequestWorkflow object from 
         * provided scheduler's array data.
         * 
         * @param array $arr : info array from scheduler status notification.
         * @return WFStatusRequestWorkflow : new WFStatusRequestWorkflow object.
         */
        public static function parseArray($arr)
        {
            $newObj = new WFStatusRequestWorkflow();
            $newObj->name = $arr['name'] ?? null;
            $newObj->auth = $arr['auth'] ?? null;
            
            if( isset($arr['steps']) && is_array($arr['steps']) )
            {
                foreach($arr['steps'] as $arrStep)
                {
                    $newStep = WFStatusRequestStep::parseArray($arrStep);
                    if($newStep)
                        $newObj->steps[] = $newStep;
                }
            }
            
            return $newObj;
        }
    };
