<?php

    namespace WorkflowManager\Workflow;
    
    class WorkflowStepDefinition {
        public $block;
        public $step;
        public $task;
        public $parameters;
        /** @var  Input[] */
        public $inputs;
        /** @var  Output[] */
        public $outputs;
    
        function __construct($step, $block) {
            $this->step = $step;
            $this->block = $block;
            $this->inputs = [];
            $this->outputs = [];
        }
    
        public function getInputValue($position) {
            // retrieve the required input path
            return $this->inputs[$position]->path;
        }
    
        public function getOutputValue($position) {
            // retrieve the required input
            return $this->outputs[$position]->path;
        }
    
        /**
         * @param $jsonString
         *
         * @return WorkflowStepDefinition
         */
        public static function _fromJsonString($jsonString) {
            return self::_fromJsonObject(json_decode($jsonString));
        }
    
        /**
         * @param $jsonStructure
         *
         * @return WorkflowStepDefinition
         */
        public static function _fromJsonObject($jsonStructure) {
            $step = new WorkflowStepDefinition($jsonStructure->step, $jsonStructure->block);
            $step->task = $jsonStructure->task;
            $step->parameters = isset($jsonStructure->parameters) ? $jsonStructure->parameters : new \stdClass();
            foreach ($jsonStructure->inputs as $o) {
                $step->inputs[] = Input::_fromJsonStructure($o);
            }
            foreach ($jsonStructure->outputs as $o) {
                $step->outputs[] = Output::_fromJsonStructure($o);
            }
    
            return $step;
        }
    }
