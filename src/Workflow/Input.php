<?php

    namespace WorkflowManager\Workflow;
    
    
    class Input {
        /**
         * @var string
         */
        public $key;
        /**
         * @var string
         */
        public $path;
        /**
         * @var string
         */
        public $type;
        /**
         * @var bool
         */
        public $temp;
        /**
         * @var bool
         */
        public $customizable;
    
        public static function _fromJsonStructure($jsonStructure) {
            $o = new Input();
            $o->key = $jsonStructure->key;
            $o->path = $jsonStructure->path;
            $o->type = $jsonStructure->type;
            $o->temp = isset($jsonStructure->temp) ? $jsonStructure->temp : true;
            $o->customizable = isset($jsonStructure->customizable) ? $jsonStructure->customizable : false;
    
            return $o;
        }
    
    }
