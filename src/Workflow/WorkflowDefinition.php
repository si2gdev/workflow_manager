<?php

    namespace WorkflowManager\Workflow;
    
    class WorkflowDefinition {
    
        public $name;
        /** @var  WorkflowStepDefinition[] */
        public $steps;
        public $auth;
    
        /**
         * WorkflowDefinition constructor.
         *
         * @param                          $name
         * @param WorkflowStepDefinition[] $steps
         */
        function __construct($name, $auth, $steps = []) {
            $this->name = $name;
            $this->auth = $auth;
            $this->addSteps($steps);
        }
    
        /**
         * Linearize all the steps in the template into an array of WorkflowStepDefinition
         *
         * @param WorkflowStepDefinition|WorkflowStepDefinition[] $step
         */
        protected function addSteps($step) {
            if (is_array($step)) {
                foreach ($step as $innerStep) {
                    $this->addSteps($innerStep);
                }
            } else {
                $this->steps[] = $step;
            }
        }
    
        /**
         * @return Output[]
         */
        public function getNonTemporaryOutputs() {
            $outputs = [];
    
            foreach ($this->steps as $s) {
                $outputs = array_merge($outputs, array_filter($s->outputs, function ($o) {
                    return !$o->temp;
                }));
            }
            return $outputs;
        }
    
        /**
         * @param $jsonString
         *
         * @return WorkflowDefinition
         */
        public
        static function _fromJsonString($jsonString) {
            return self::_fromJsonObject(json_decode($jsonString));
        }
    
        /**
         * @param $jsonStructure
         *
         * @return WorkflowDefinition
         */
        public static function _fromJsonObject($jsonStructure) {
    
            $wfDef = new WorkflowDefinition($jsonStructure->name);
            foreach ($jsonStructure->steps as $step) {
                $wfDef->addSteps(WorkflowStepDefinition::_fromJsonObject($step));
            }
    
            return $wfDef;
        }
    
    }
