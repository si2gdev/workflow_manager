<?php

    namespace WorkflowManager\Workflow;
    
    class SchedulerStatus {
        public $receiver;
        public $uploader;
        public $executor;
    
        /**
         * SchedulerStatus constructor.
         *
         * @param $jsonObject
         */
        public function __construct($jsonObject) {
            $this->receiver = $jsonObject->receiver;
            $this->uploader = $jsonObject->uploader;
            $this->executor = $jsonObject->executor;
        }
    
        public function setMineOthersCounters($mineRunningWFs, $mineEnqueuedWFs, $othersRunningWFs, $othersEnqueuedWFs) {
            $this->executor->mineRunningWFs = $mineRunningWFs;
            $this->executor->mineEnqueuedWFs = $mineEnqueuedWFs;
            $this->executor->othersRunningWFs = $othersRunningWFs;
            $this->executor->othersEnqueuedWFs = $othersEnqueuedWFs;
        }
    }
