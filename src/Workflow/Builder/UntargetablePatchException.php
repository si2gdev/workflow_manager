<?php

    namespace WorkflowManager\Workflow\Builder;
    
    use Exception;
    
    class UntargetablePatchException extends Exception{
    
        public $patch;
    
        function __construct($patch) {
            $this->patch = $patch;
        }
    
    
    }
