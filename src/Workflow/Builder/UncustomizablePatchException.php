<?php

    namespace WorkflowManager\Workflow\Builder;
    
    
    class UncustomizablePatchException extends \Exception {
    
        protected $type;
    
        /**
         * UncustomizablePatchException constructor.
         *
         * @param string     $type
         * @param int        $key
         * @param \Exception $value
         *
         */
        public function __construct($type = "input", $key, $value) {
            $this->type = $type;
            parent::__construct("Uncustomizable $type. " . $key . " @ " . $value);
        }
    }
