<?php

    namespace WorkflowManager\Workflow\Builder;
    
    use Exception;
    
    class MissingInputOutputException extends Exception {
        public $step;
        public $io;
    
        /**
         * MissingInputOutputException constructor.
         *
         * @param $io
         */
        public function __construct($step, $io) {
            $this->step = $step;
            $this->io = $io;
        }
    
    
    }
