<?php

    namespace WorkflowManager\Workflow\Builder;
    
    
    use Exception;
    
    class UnknownPatchTypeException extends Exception{
        public $patch;
    
        function __construct($patch) {
            $this->patch = $patch;
        }
    
    
    }
