<?php

    namespace WorkflowManager\Workflow\Builder;
    
    use WorkflowManager\Workflow\WorkflowDefinition;
    use WorkflowManager\Workflow\WorkflowStepDefinition;
    use WorkflowManager\Workflow\Template\WorkflowTemplateDefinition;
    use WorkflowManager\Workflow\Template\WorkflowTemplateStepDefinition;
    use WorkflowManager\Configuration\Env;
    
    class WorkflowBuilder {
        const OUT_VARIABLE_REGEX = "/<([^>]+)_INDEX>/";
        public static $specialPathWorkflowBasePath;
        public static $specialPathWorkflowFolderPlaceholder;
        public static $specialPathWorkflowUUIDPlaceholder;
    
        /** @var string */
        private $workflowTemplate;
        /** @var  array */
        private $customizations;
    
        function __construct($workflowTemplate, $customizations = array()) {
            $this->workflowTemplate = $workflowTemplate;
            $this->customizations = $customizations;
    
            if ( empty(self::$specialPathWorkflowFolderPlaceholder) ) {
                if( !empty(Env::$SPECIAL_PATH_WF_FOLDER_PLACEHOLDER) ) {
                    self::$specialPathWorkflowFolderPlaceholder = Env::$SPECIAL_PATH_WF_FOLDER_PLACEHOLDER;
                }
                else
                    throw new \Exception("Builder not configured: you must specify the specialPathWorkflowFolderPlaceholder");
            }
            if ( empty(self::$specialPathWorkflowUUIDPlaceholder) ) {
                if( !empty(Env::$SPECIAL_PATH_WF_UUID_PLACEHOLDER) ) {
                    self::$specialPathWorkflowUUIDPlaceholder = Env::$SPECIAL_PATH_WF_UUID_PLACEHOLDER;
                }
                else
                    throw new \Exception("Builder not configured: you must specify the specialPathWorkflowUUIDPlaceholder");
            }
            if ( empty(self::$specialPathWorkflowBasePath) ) {
                if( !empty(Env::$WORKFLOW_BASE_FOLDER) ) {
                    self::$specialPathWorkflowBasePath = Env::$WORKFLOW_BASE_FOLDER;
                }
                else
                    throw new \Exception("Builder not configured: you must specify the specialPathWorkflowBasePath");
            }
        }
    
        /**
         * According to the notes written on 09-jun-2015 17:00, create the WF-Builder that accepts a WF-Template and a list
         * of patches to apply it.
         *
         * @param $uuid
         *
         * @return WorkflowDefinition if the Template could not be found on DB.
         * @throws MissingInputOutputException
         * @throws UnknownPatchTypeException
         * @throws UntargetablePatchException
         */
        public function createWorkflow($uuid, $auth) {
            // parse json definition of the template
            $templateDefinition = WorkflowTemplateDefinition::_fromJsonString($this->workflowTemplate);
           
            // apply eventually patches
            foreach ($this->customizations as $customization) {
                $this->applyPatchToTemplateDefinition($templateDefinition, $customization);
            }
    
            // build each WorkflowStep from WFStep-T
            $concreteSteps = [];
            foreach ($templateDefinition->steps as $step) {
                $concreteSteps[] = $this->buildWorkflowStep($step, $uuid);
            }
    
            // convert WorkflowTemplateDefinition to WorkflowDefinition
            return new WorkflowDefinition($templateDefinition->name, $auth, $concreteSteps);
        }
    
    
        /**
         * @param WorkflowTemplateStepDefinition $stepTemplateDefinition
         *
         * @param                                $uuid
         *
         * @return WorkflowStepDefinition
         * @throws MissingInputOutputException
         */
        private function buildWorkflowStep(WorkflowTemplateStepDefinition $stepTemplateDefinition, $uuid) {
            $step = new WorkflowStepDefinition($stepTemplateDefinition->step, $stepTemplateDefinition->block);
            $step->task = $stepTemplateDefinition->task;
            // get each non empty input from the WF-T
            $step->inputs = $stepTemplateDefinition->retrieveFinalIOs($stepTemplateDefinition->inputs);
            // get each non empty output from the WF-T
            $step->outputs = $stepTemplateDefinition->retrieveFinalIOs($stepTemplateDefinition->outputs);
    
            // Each file with special path must be saved inside the WF directory (named with the WF uuid)
            foreach ($step->inputs as $input) {
                $input->path = $this->transformSpecialPath($input->path, $uuid);
            }
    
            // check for multi-outputs
            $concreteOutputs = [];
            foreach ($step->outputs as $output) {
                $output->path = $this->transformSpecialPath($output->path, $uuid);
                $outputs = $this->convertMultipleOutputs($output, $step->inputs);
                foreach ($outputs as $o) {
                    $concreteOutputs[] = $o;
                }
            }
            $step->outputs = $concreteOutputs;
    
            // list of key/value pairs
            $par = [];
            foreach ($stepTemplateDefinition->parameters as $p) {
                $par[$p->getName()] = $p->getValue();
            }
            // convert from array to stdClass
            $step->parameters = (object)$par;
    
            return $step;
        }
    
        /**
         * @param string $path   Path to be transformed.
         * @param string $wfUuid UUID of the Workflow.
         *
         * @return string
         */
        private function transformSpecialPath($path, $wfUuid) {
            $str = str_replace(WorkflowBuilder::$specialPathWorkflowFolderPlaceholder,
                WorkflowBuilder::$specialPathWorkflowBasePath . "/" . $wfUuid,
                $path);
            return str_replace(self::$specialPathWorkflowUUIDPlaceholder,
                $wfUuid,
                $str);
        }
    
        /**
         * @param WorkflowTemplateDefinition $templateDef
         * @param                            $patch
         *
         * @throws UncustomizablePatchException
         * @throws UnknownPatchTypeException
         * @throws UntargetablePatchException
         */
        private function applyPatchToTemplateDefinition(WorkflowTemplateDefinition $templateDef, \stdClass $patch) {
            $foundStep = $templateDef->getStep($patch->stepId);
    
            if ($foundStep == null)
                throw new UntargetablePatchException($patch);
    
            if (isset($patch->inputs)) {
                // patch requires to customize the inputs
                foreach ($patch->inputs as $customInput) {
                    $foundStep->customizeInput($customInput);
                }
            }
            if (isset($patch->outputs)) {
                // patch requires to customize the outputs
                foreach ($patch->outputs as $customOutput) {
                    $foundStep->customizeOutput($customOutput);
                }
            }
    
            if (isset($patch->parameters)) {
                // patch requires to customize the parameters
                foreach ($patch->parameters as $key => $value) {
                    $foundStep->customizeParameter($key, $value);
                }
            }
    
            if (!isset($patch->parameters) && !isset($patch->inputs) && !isset($patch->outputs)) {
                // unknown patch type
                throw new UnknownPatchTypeException($patch);
            }
        }
    
        private function convertMultipleOutputs($output, $inputs) {
            // Search for well-known syntax <Variable_INDEX>
            preg_match(self::OUT_VARIABLE_REGEX, $output->path, $output_array);
    
            if (count($output_array) > 1) {
                // this output path contains a variable, so search the variable into the input keys
                $numberOfInputs = 0;
                $toReplace = $output_array[0];
                $variableToSearch = $output_array[1];
    
                // Search the variable inside the input keys and annotate the number of paths defined by the found input
                foreach ($inputs as $input) {
                    if ($input->key == $variableToSearch) {
                        $numberOfInputs = count($input->path);
                    }
                }
    
                $newOutputs = [];
                for ($i = 1; $i <= $numberOfInputs; $i++) {
                    $tmp = clone $output;
                    $tmp->path = str_replace($toReplace, $i, $output->path);
                    $newOutputs[] = $tmp;
                }
    
                return $newOutputs;
            } else {
                // plain path, it needs no transformations
                return [$output];
            }
        }
    }
