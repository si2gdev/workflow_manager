<?php

    namespace WorkflowManager\Workflow;
    
    
    class UnknownStepDefinitionException extends \Exception {
    
        public $task;
    
    
        /**
         * UnknownStepDefinitionException constructor.
         *
         * @param string $step
         */
        public function __construct($step) { $this->task = $step; }
    }
