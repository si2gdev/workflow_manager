<?php

    namespace WorkflowManager\Workflow\Template;

    class FreeStringParameter extends AbstractParameter {
        const TYPE = "string";
    
        /**
         * FreeStringParameter constructor.
         *
         * @param $name
         * @param $label
         * @param $default
         * @param $visible
         * @param $required
         * @param $editableFromUI
         */
        public function __construct($name, $label, $default, $visible, $required, $editableFromUI) {
            parent::__construct($name, self::TYPE, $label, $default, $visible, $required, $editableFromUI);
        }
    }
