<?php

    namespace WorkflowManager\Workflow\Template;
    
    class ParameterFactory {
    
        /**
         * @param $parameterJson
         *
         * @return AbstractParameter
         * @throws \Exception
         */
        public static function createParameter($parameterJson) {
            $required = isset($parameterJson->required) ? $parameterJson->required : false;
            $visible = isset($parameterJson->visible) ? $parameterJson->visible : true;
            $editableFromUI = isset($parameterJson->editableFromUI) ? $parameterJson->editableFromUI : true;
            $min = isset($parameterJson->min) ? $parameterJson->min : null;
            $max = isset($parameterJson->max) ? $parameterJson->max : null;
    
            switch ($parameterJson->type) {
                case BooleanParameter::TYPE:
                    return new BooleanParameter($parameterJson->name,
                        $parameterJson->label,
                        $parameterJson->default,
                        $visible,
                        $required,
                        $editableFromUI);
                case FreeStringParameter::TYPE:
                    return new FreeStringParameter($parameterJson->name,
                        $parameterJson->label,
                        $parameterJson->default,
                        $visible,
                        $required,
                        $editableFromUI);
                case NumberParameter::TYPE:
                    return new NumberParameter($parameterJson->name,
                        $parameterJson->label,
                        $parameterJson->default,
                        $visible,
                        $required,
                        $editableFromUI,
                        $min,
                        $max);
                case SingleChoiceParameter::TYPE:
                    // parse choices
                    $choices = [];
                    if (isset($parameterJson->choices)) {
                        foreach ($parameterJson->choices as $c) {
                            $choices[] = new SingleChoiceItem($c->value, $c->label);
                        }
                    }
                    return new SingleChoiceParameter($parameterJson->name,
                        $parameterJson->label,
                        $parameterJson->default,
                        $visible,
                        $required,
                        $editableFromUI,
                        $choices);
                default:
                    throw new \Exception("Unknown parameter type");
            }
        }
    
        /**
         * @param $step
         *
         * @return AbstractParameter[]
         * @throws \Exception
         */
        public static function createParameters($step) {
            if (isset($step->parameters)) {
                $parameters = [];
                foreach ($step->parameters as $p) {
                    $parameters[] = self::createParameter($p);
                }
    
                return $parameters;
            } else {
                return [];
            }
        }
    }
