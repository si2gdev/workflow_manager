<?php

    namespace WorkflowManager\Workflow\Template;
    
    class SingleChoiceItem {
        protected $value, $label;
    
        /**
         * SingleChoiceItem constructor.
         *
         * @param $value
         * @param $label
         */
        public function __construct($value, $label) {
            $this->value = $value;
            $this->label = $label;
        }
    
        /**
         * @return mixed
         */
        public function getValue() {
            return $this->value;
        }
    
        /**
         * @return mixed
         */
        public function getLabel() {
            return $this->label;
        }
    
    
    }
