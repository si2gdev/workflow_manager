<?php

    namespace WorkflowManager\Workflow\Template;
    
    use WorkflowManager\IO\SAL;
    use WorkflowManager\Workflow\UnknownParameterException;
    use WorkflowManager\Workflow\Builder\MissingInputOutputException;
    use WorkflowManager\Workflow\Builder\UncustomizablePatchException;
    
    class WorkflowTemplateStepDefinition {
    
        public $block;
        public $step;
        public $task;
        /** @var  array */
        public $inputs;
        /** @var AbstractParameter[] */
        public $parameters;
        /** @var  array */
        public $outputs;
    
        private $customizedIOs;
    
        function __construct($step, $block) {
            $this->step = $step;
            $this->block = $block;
            $this->parameters = array();
            $this->inputs = array();
            $this->outputs = array();
    
            $this->customizedIOs = new \SplObjectStorage();
        }
    
        /**
         * @param $customInput
         *
         * @throws UncustomizablePatchException
         * @throws \Exception
         */
        public function customizeInput($customInput) {
            $uncustomizable = true;
    
            // search for an input with the key specified in the patch
            foreach ($this->inputs as $input) {
                if ($input->key == $customInput->key && // if this input has the patched key
                    $input->customizable // and it's customizable
                ) {
                    // check cardinality
                    $cardinality = new StepInputOutputCardinality($input);
    
                    if ($cardinality->getMax() <= 1) {
                        // single input
                        $input->path = SAL::sanitizeURI($customInput->value);
                    } else {
                        // multiple input
    
                        if (!is_array($input->path)) {
                            // path wasn't an array yet, create it now
                            $input->path = [];
                        }
    
                        // append current value, if possible
                        if ($cardinality->getMax() <= count($input->path))
                            throw new \Exception("Cardinality exception: you cannot specify more inputs. Reached max is " . $cardinality->getMax());
    
                        $input->path[] = SAL::sanitizeURI($customInput->value);
                    }
    
                    $uncustomizable = false;
                    // take annotation that we have customized this input
                    $this->customizedIOs[$input] = $customInput;
                }
            }
    
            if ($uncustomizable)
                throw new UncustomizablePatchException("Input", $customInput->key, $customInput->value);
        }
    
        public function customizeOutput($customOutput) {
            $uncustomizable = true;
    
            // search for an output with the key specified in the patch
            foreach ($this->outputs as $output) {
                if ($output->key == $customOutput->key && // if this output has the patched key
                    $output->customizable // and it's customizable
                ) {
                    // overwrite its value
                    $output->path = SAL::sanitizeURI($customOutput->value);
                    $uncustomizable = false;
                    // take annotation that we have customized this output
                    $this->customizedIOs[$output] = $customOutput;
                }
            }
            if ($uncustomizable)
                throw new UncustomizablePatchException("Output", $customOutput->key, $customOutput->value);
        }
    
        public function customizeParameter($parameterKey, $parameterValue) {
            $applied = false;
            foreach ($this->parameters as $p) {
                if ($p->getName() == $parameterKey) {
                    $applied = true;
                    // if this is the parameter to which apply the patch, check if the value required is allowed.
                    if ($p->allowedValue($parameterValue)) {
                        $p->setValue($parameterValue);
                    } else {
                        // raise an ex
                        throw new UncustomizablePatchException("Parameter", $parameterKey, $parameterValue);
                    }
                }
            }
            if (!$applied)
                throw new UnknownParameterException($parameterKey);
        }
    
        /**
         * Fetch the list of given IOs and retrieve only those fixed and customized: if an IO is optional and not provided
         * it is excluded from the results.
         *
         * @param $IOs
         *
         * @return array
         * @throws MissingInputOutputException
         */
        public function retrieveFinalIOs($IOs) {
            $results = [];
            foreach ($IOs as $IO) {
                if ($this->isIOProvided($IO)) {
                    // if the input is provided, include it
                    $results[] = $IO;
                } else {
                    $cardinality = new StepInputOutputCardinality($IO);
                    if (!$cardinality->isOptional()) {
                        // this input is not provided and it's not optional, raise an exception
                        throw new MissingInputOutputException($this->step, $IO);
                    } else {
                        // this input is not provided but it's optional, so just skip it
                    }
                }
            }
            return $results;
        }
    
        public function isIOProvided($input) {
            return !$input->customizable || $this->customizedIOs->contains($input);
        }
    }
