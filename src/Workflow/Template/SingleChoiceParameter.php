<?php

    namespace WorkflowManager\Workflow\Template;
    
    class SingleChoiceParameter extends AbstractParameter {
    
        const TYPE = "singleChoice";
        /**
         * @var SingleChoiceItem[]
         */
        protected $choices;
    
        public function __construct($name, $label, $default, $visible, $required, $editableFromUI, array $choices = null) {
            parent::__construct($name, self::TYPE, $label, $default, $visible, $required, $editableFromUI);
            $this->choices = $choices;
        }
    
        /**
         * @return SingleChoiceItem[]
         */
        public function getChoices() {
            return $this->choices;
        }
    
        public function allowedValue($value) {
            if (!parent::allowedValue($value))
                return false;
    
            foreach ($this->choices as $choice) {
                if ($value == $choice->getValue())
                    return true;
            }
    
            return false;
        }
    
    }
