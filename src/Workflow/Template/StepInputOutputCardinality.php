<?php

    namespace WorkflowManager\Workflow\Template;
    
    
    class StepInputOutputCardinality {
        /** @var int */
        protected $min, $max;
    
        /**
         * StepInputOutputCardinality constructor.
         *
         * @param \stdClass $value
         *
         * @throws \Exception
         */
        public function __construct(\stdClass $value) {
            $cardinality = isset($value->cardinality) ? $value->cardinality : "1-1";
            $this->parseCardinalityString($cardinality);
        }
    
        protected function parseCardinalityString($cardString) {
            $elements = explode("-", $cardString);
            if (count($elements) != 2)
                throw new \Exception("Bad cardinality expression");
    
            if (filter_var($elements[0], FILTER_VALIDATE_INT) === false)
                throw new \Exception("Bad cardinality expression: left value is not integer");
    
            if (filter_var($elements[1], FILTER_VALIDATE_INT) === false
                && $elements[1] != "n"
            )
                throw new \Exception("Bad cardinality expression: right value is not integer nor 'n'");
    
            $this->min = intval($elements[0]);
            $this->max = $elements[1] == "n" ? PHP_INT_MAX : intval($elements[1]);
    
            if ($this->min < 0)
                throw new \Exception("Bad cardinality values: left value is negative.");
            if ($this->min > $this->max)
                throw new \Exception("Bad cardinality values: right value MUST be greater/equal to left one.");
        }
    
        public function getMin() {
            return $this->min;
        }
    
        public function getMax() {
            return $this->max;
        }
    
        public function isOptional() {
            return $this->min == 0;
        }
    }
