<?php

    namespace WorkflowManager\Workflow\Template;
    
    use WorkflowManager\Workflow\UnknownStepDefinitionException;
    
    class WorkflowTemplateDefinition {
    
        public $name;
        /** @var  WorkflowTemplateStepDefinition[] */
        public $steps;
    
        function __construct() {
            $this->steps = [];
        }
    
        /**
         * @param $stepId
         *
         * @return WorkflowTemplateStepDefinition
         */
        public function getStep($stepId) {
            foreach ($this->steps as $step) {
                if ($step->step == $stepId) {
                    return $step;
                }
            }
    
            //  step not found in the template
            return null;
        }
    
        /**
         * @param $step
         *
         * @return WorkflowTemplateStepDefinition
         * @throws UnknownStepDefinitionException
         */
        protected function createStepDefinition($step) {
            $stepDef = new WorkflowTemplateStepDefinition($step->step, $step->block);
            // TODO: the check of specific steps now is unuseful: it's too restrictive. So, now, just apply the TASK specified in the DB.
            $stepDef->task = $step->task;
    
            $stepDef->inputs = $step->inputs;
            $stepDef->outputs = $step->outputs;
            // convert to AbstractParameters objects (from plain JSON structure)
            $stepDef->parameters = ParameterFactory::createParameters($step);
            // and apply each default as base
            foreach ($stepDef->parameters as $p) {
                $p->applyDefaultIfAny();
            }
    
            return $stepDef;
        }
    
        protected function addSingleStep(WorkflowTemplateStepDefinition $step) {
            $this->steps[] = $step;
        }
    
        protected function addConcurrentSteps($steps = array()) {
            // add the steps array as a whole
            $this->steps[] = $steps;
        }
    
        /**
         * Use $jsonStructure to build the template.
         *
         * @param string $jsonString
         *
         * @return WorkflowTemplateDefinition
         * @throws UnknownStepDefinitionException
         */
        public static function _fromJsonString($jsonString) {
            $jsonStructure = json_decode($jsonString);
            $template = new WorkflowTemplateDefinition();
    
            $template->name = $jsonStructure->name;
            foreach ($jsonStructure->steps as $step) {
                if (is_array($step)) {
                    // create the list of specified step-tasks
                    $concurrentSteps = array();
                    foreach ($step as $innerStep) {
                        $concurrentSteps[] = $template->createStepDefinition($innerStep);
                    }
                    // then add them to the list
                    $template->addConcurrentSteps($concurrentSteps);
                } else {
                    // create specified step-task
                    $stepDef = $template->createStepDefinition($step);
                    // then add it to the list
                    $template->addSingleStep($stepDef);
                }
            }
            return $template;
        }
    }
