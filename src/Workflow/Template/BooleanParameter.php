<?php

    namespace WorkflowManager\Workflow\Template;
    
    
    class BooleanParameter extends AbstractParameter {
        const TYPE = "boolean";
    
        /**
         * FreeStringParameter constructor.
         *
         * @param $name
         * @param $label
         * @param $default
         * @param $visible
         * @param $required
         * @param $editableFromUI
         */
        public function __construct($name, $label, $default, $visible, $required, $editableFromUI) {
            parent::__construct($name, self::TYPE, $label, $default, $visible, $required, $editableFromUI);
        }
    }
