<?php

    namespace WorkflowManager\Workflow\Template;
    
    
    class AbstractParameter {
        protected $name;
        protected $type;
        protected $label;
        protected $default;
        protected $visible;
        protected $required;
        protected $editableFromUI;
        protected $value;
    
        /**
         * AbstractParameter constructor.
         *
         * @param $name
         * @param $type
         * @param $label
         * @param $default
         * @param $visible
         * @param $required
         * @param $editableFromUI
         */
        public function __construct($name, $type, $label, $default, $visible, $required, $editableFromUI) {
            $this->name = $name;
            $this->type = $type;
            $this->label = $label;
            $this->default = $default;
            $this->visible = $visible;
            $this->required = $required;
            $this->editableFromUI = $editableFromUI;
        }
    
        public function applyDefaultIfAny(){
            if($this->value == null){
                $this->value = $this->default;
            }
        }
    
        /**
         * @return mixed
         */
        public function getName() {
            return $this->name;
        }
    
        /**
         * @return mixed
         */
        public function getType() {
            return $this->type;
        }
    
        /**
         * @return mixed
         */
        public function getLabel() {
            return $this->label;
        }
    
        /**
         * @return mixed
         */
        public function getDefault() {
            return $this->default;
        }
    
        /**
         * @return mixed
         */
        public function getVisible() {
            return $this->visible;
        }
    
        /**
         * @return mixed
         */
        public function getRequired() {
            return $this->required;
        }
    
        /**
         * @return mixed
         */
        public function getEditableFromUI() {
            return $this->editableFromUI;
        }
    
        public function allowedValue($value) {
            return true;
        }
    
        /**
         * @return mixed
         */
        public function getValue() {
            return $this->value;
        }
    
        /**
         * @param mixed $value
         */
        public function setValue($value) {
            $this->value = $value;
        }
    
    
    }
