<?php

    namespace WorkflowManager\Workflow\Template;
    
    
    class NumberParameter extends AbstractParameter {
        const TYPE = "number";
    
        protected $min;
        protected $max;
    
        public function __construct($name, $label, $default, $visible, $required, $editableFromUI, $min, $max) {
            parent::__construct($name, self::TYPE, $label, $default, $visible, $required, $editableFromUI);
            $this->min = $min;
            $this->max = $max;
        }
    
        /**
         * @return mixed
         */
        public function getMin() {
            return $this->min;
        }
    
        /**
         * @return mixed
         */
        public function getMax() {
            return $this->max;
        }
    
        public function allowedValue($value) {
            return parent::allowedValue($value) &&
            ($value >= $this->min || !$this->min) && // value >= the min, or min not set
            ($value <= $this->max || !$this->max); // value <= the max, or max not set
        }
    
    }
