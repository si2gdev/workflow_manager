<?php

require __DIR__ . '/../../../../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

/**
 * WorkflowBuilder test case.
 */
class WorkflowBuilderTest extends TestCase
{

    /**
     *
     * @var WorkflowManager\Workflow\Builder\WorkflowBuilder
     */
    private $workflowBuilder;

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->workflowBuilder = null;
        
        parent::tearDown();
    }

    /**
     * Prepares the environment before running a test.
     */
    public function setUp()
    {
        parent::setUp();
        
        $jsonWfTemplate = <<<WF_TEMPLATE
{
    "name": "Import",
    "type": "dataset-creation",
    "steps": [
        {
            "block": "1",
            "step": "1",
            "task": "DataRetrieve",
            "inputs": [],
            "parameters": [
                {
                    "name": "uri",
                    "label": "uri",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "audience",
                    "label": "audience",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "callback",
                    "label": "callback",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "authorization",
                    "label": "authorization",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                }		
            ],
            "outputs": []
        },
        {
            "block": "2",
            "step": "2",
            "task": "DataIngest",
            "inputs": [],
            "parameters": [
                {
                    "name": "sessionId",
                    "label": "sessionId",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "companyId",
                    "label": "companyId",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "userId",
                    "label": "userId",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "callback",
                    "label": "callback",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "authorization",
                    "label": "authorization",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                }
            ],
            "outputs": []
        },
        {
            "block": "3",
            "step": "3",
            "task": "DataPublish",
            "inputs": [],
            "parameters": [
                {
                    "name": "publishUri",
                    "label": "publishUri",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "callback",
                    "label": "callback",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                },
                {
                    "name": "authorization",
                    "label": "authorization",
                    "type": "string",
                    "default": "",
                    "visible": true,
                    "editableFromUI": true
                }
            ],
            "outputs": []
        }
    ]
}
WF_TEMPLATE;
        
        $jsonWfCustomizations = <<<WF_CUSTOMIZATIONS
[
	{
		"stepId": "1",
		"parameters": {
			"uri": "http://s3.amazonaws.com/public.si2g/input.zip",
			"audience": "https://ta-gamete-api-dev.azure-api.net",
			"callback": "http://127.0.0.1/importer/127",
			"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5rUkNSRFJFUlRZeE9FUTBNamN4T0RnNVEwTkJSVU5ETXpkR09UUTBOa1V3T1RCRU16ZEJRdyJ9.eyJodHRwczovL2dhbWV0ZS5jb20vYXBwX21ldGFkYXRhIjp7ImNvbXBhbnlJZCI6IjRjMzE5ZjM4LTYwYjYtNGMwMi04ZDRlLWExYjU3YWIzYjExMCIsInJvbGVzIjpbIkFkbWluIl0sInVzZXJJZCI6IjQ0YzhjNTQxLTYxYmMtNDE5Mi1hZGE2LTZmZjNmMzQ1YWFkYiIsImJyYW5kIjoidGllcnJhIn0sImh0dHBzOi8vdGEtZ2FtZXRlLWFwaS1kZXYuYXp1cmUtYXBpLm5ldC9hcHBfbWV0YWRhdGEiOnsiY29tcGFueUlkIjoiNGMzMTlmMzgtNjBiNi00YzAyLThkNGUtYTFiNTdhYjNiMTEwIiwicm9sZXMiOlsiQWRtaW4iXSwidXNlcklkIjoiNDRjOGM1NDEtNjFiYy00MTkyLWFkYTYtNmZmM2YzNDVhYWRiIiwiYnJhbmQiOiJ0aWVycmEifSwiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0L3VzZXJfbWV0YWRhdGEiOnsiZXhwaXJlcyI6IiIsInBob25lIjoiIiwic3VybmFtZSI6IlNwb3RsaWdodCIsIm5hbWUiOiJEZXZVc2VyIiwibmlja25hbWUiOiI0NGM4YzU0MS02MWJjLTQxOTItYWRhNi02ZmYzZjM0NWFhZGIiLCJsb2NhbGUiOiJlbiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfSwiaXNzIjoiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YTc0MzAzMDBhNmI0MTcxNWFlYTczODMiLCJhdWQiOlsiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0IiwiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MjgyOTc4OTgsImV4cCI6MTUyODM4NDI5OCwiYXpwIjoiQ2I1T0s1SUJzNG9QOVpDZXlXU0M0MGNEcjZpU3VnQWoiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCJ9.Ceocfk9j1BjcwQbaZVYSWoUdvqzDgPjQTqpKSPiJD1IPey9WFLwYTzK4OMpK8HJpRZ34-MtzHwnE2qT5DBV4cFrGu1nu7LA3QDjS2gvQbbMktS-JwRNwN3Y_Ce7paiR3e9aeNSmbEFdvx173OIGt-w5poHio2bweVTMFFMHcu64Q3JyjU55El_Nk1L2nevh2fW7w2_ByP_vBsHqFaWWJEYKIw9I4HtSBnGojMUXSwT2qptDe5fkPwXbhg13jGlxBeFWIS7bi4VcArbIao9N0RQVHYj-yMX7OYWtgPjbMXlOa1jOhZTm67Wd38NkMYtu_BbAXjD1bJbiIyg7cqTF1xg"
		}
	},
	{
		"stepId": "2",
		"parameters": {
			"sessionId": "127",
			"companyId": "4c319f38-60b6-4c02-8d4e-a1b57ab3b110",
			"userId": "44c8c541-61bc-4192-ada6-6ff3f345aadb",
			"callback": "http://127.0.0.1/importer/127",
			"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5rUkNSRFJFUlRZeE9FUTBNamN4T0RnNVEwTkJSVU5ETXpkR09UUTBOa1V3T1RCRU16ZEJRdyJ9.eyJodHRwczovL2dhbWV0ZS5jb20vYXBwX21ldGFkYXRhIjp7ImNvbXBhbnlJZCI6IjRjMzE5ZjM4LTYwYjYtNGMwMi04ZDRlLWExYjU3YWIzYjExMCIsInJvbGVzIjpbIkFkbWluIl0sInVzZXJJZCI6IjQ0YzhjNTQxLTYxYmMtNDE5Mi1hZGE2LTZmZjNmMzQ1YWFkYiIsImJyYW5kIjoidGllcnJhIn0sImh0dHBzOi8vdGEtZ2FtZXRlLWFwaS1kZXYuYXp1cmUtYXBpLm5ldC9hcHBfbWV0YWRhdGEiOnsiY29tcGFueUlkIjoiNGMzMTlmMzgtNjBiNi00YzAyLThkNGUtYTFiNTdhYjNiMTEwIiwicm9sZXMiOlsiQWRtaW4iXSwidXNlcklkIjoiNDRjOGM1NDEtNjFiYy00MTkyLWFkYTYtNmZmM2YzNDVhYWRiIiwiYnJhbmQiOiJ0aWVycmEifSwiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0L3VzZXJfbWV0YWRhdGEiOnsiZXhwaXJlcyI6IiIsInBob25lIjoiIiwic3VybmFtZSI6IlNwb3RsaWdodCIsIm5hbWUiOiJEZXZVc2VyIiwibmlja25hbWUiOiI0NGM4YzU0MS02MWJjLTQxOTItYWRhNi02ZmYzZjM0NWFhZGIiLCJsb2NhbGUiOiJlbiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfSwiaXNzIjoiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YTc0MzAzMDBhNmI0MTcxNWFlYTczODMiLCJhdWQiOlsiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0IiwiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MjgyOTc4OTgsImV4cCI6MTUyODM4NDI5OCwiYXpwIjoiQ2I1T0s1SUJzNG9QOVpDZXlXU0M0MGNEcjZpU3VnQWoiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCJ9.Ceocfk9j1BjcwQbaZVYSWoUdvqzDgPjQTqpKSPiJD1IPey9WFLwYTzK4OMpK8HJpRZ34-MtzHwnE2qT5DBV4cFrGu1nu7LA3QDjS2gvQbbMktS-JwRNwN3Y_Ce7paiR3e9aeNSmbEFdvx173OIGt-w5poHio2bweVTMFFMHcu64Q3JyjU55El_Nk1L2nevh2fW7w2_ByP_vBsHqFaWWJEYKIw9I4HtSBnGojMUXSwT2qptDe5fkPwXbhg13jGlxBeFWIS7bi4VcArbIao9N0RQVHYj-yMX7OYWtgPjbMXlOa1jOhZTm67Wd38NkMYtu_BbAXjD1bJbiIyg7cqTF1xg"
		}
	},
	{
		"stepId": "3",
		"parameters": {
			"publishUri": "http://127.0.0.1/importer/127/callback",
			"callback": "http://127.0.0.1/importer/127",
			"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5rUkNSRFJFUlRZeE9FUTBNamN4T0RnNVEwTkJSVU5ETXpkR09UUTBOa1V3T1RCRU16ZEJRdyJ9.eyJodHRwczovL2dhbWV0ZS5jb20vYXBwX21ldGFkYXRhIjp7ImNvbXBhbnlJZCI6IjRjMzE5ZjM4LTYwYjYtNGMwMi04ZDRlLWExYjU3YWIzYjExMCIsInJvbGVzIjpbIkFkbWluIl0sInVzZXJJZCI6IjQ0YzhjNTQxLTYxYmMtNDE5Mi1hZGE2LTZmZjNmMzQ1YWFkYiIsImJyYW5kIjoidGllcnJhIn0sImh0dHBzOi8vdGEtZ2FtZXRlLWFwaS1kZXYuYXp1cmUtYXBpLm5ldC9hcHBfbWV0YWRhdGEiOnsiY29tcGFueUlkIjoiNGMzMTlmMzgtNjBiNi00YzAyLThkNGUtYTFiNTdhYjNiMTEwIiwicm9sZXMiOlsiQWRtaW4iXSwidXNlcklkIjoiNDRjOGM1NDEtNjFiYy00MTkyLWFkYTYtNmZmM2YzNDVhYWRiIiwiYnJhbmQiOiJ0aWVycmEifSwiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0L3VzZXJfbWV0YWRhdGEiOnsiZXhwaXJlcyI6IiIsInBob25lIjoiIiwic3VybmFtZSI6IlNwb3RsaWdodCIsIm5hbWUiOiJEZXZVc2VyIiwibmlja25hbWUiOiI0NGM4YzU0MS02MWJjLTQxOTItYWRhNi02ZmYzZjM0NWFhZGIiLCJsb2NhbGUiOiJlbiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfSwiaXNzIjoiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YTc0MzAzMDBhNmI0MTcxNWFlYTczODMiLCJhdWQiOlsiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0IiwiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MjgyOTc4OTgsImV4cCI6MTUyODM4NDI5OCwiYXpwIjoiQ2I1T0s1SUJzNG9QOVpDZXlXU0M0MGNEcjZpU3VnQWoiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCJ9.Ceocfk9j1BjcwQbaZVYSWoUdvqzDgPjQTqpKSPiJD1IPey9WFLwYTzK4OMpK8HJpRZ34-MtzHwnE2qT5DBV4cFrGu1nu7LA3QDjS2gvQbbMktS-JwRNwN3Y_Ce7paiR3e9aeNSmbEFdvx173OIGt-w5poHio2bweVTMFFMHcu64Q3JyjU55El_Nk1L2nevh2fW7w2_ByP_vBsHqFaWWJEYKIw9I4HtSBnGojMUXSwT2qptDe5fkPwXbhg13jGlxBeFWIS7bi4VcArbIao9N0RQVHYj-yMX7OYWtgPjbMXlOa1jOhZTm67Wd38NkMYtu_BbAXjD1bJbiIyg7cqTF1xg"
		}
	}
]
WF_CUSTOMIZATIONS;
        $wfCustomizations = json_decode($jsonWfCustomizations);
        
        // $wfTemplate is expected as JSON.
        // $wfCustomizations is expected as array.
        $this->workflowBuilder = new WorkflowManager\Workflow\Builder\WorkflowBuilder($jsonWfTemplate, $wfCustomizations);
    }

    /**
     * Tests WorkflowBuilder->createWorkflow()
     */
    public function testCreateWorkflow()
    {
        $wfUuid = 'de3c597a-85ee-44ce-a044-ba07865ab906';
        $wfAuth = 'f53147f6-08ae-469c-be9e-8b79055c0889';
        
        $wfDefinition = $this->workflowBuilder->createWorkflow($wfUuid, $wfAuth);
        $testWfDefinition = json_decode(json_encode($wfDefinition), true);
        
        $expectedWfDefinition = json_decode(<<<WF_DEFINITION
{
	"name": "Import",
	"steps": [{
			"block": "1",
			"step": "1",
			"task": "DataRetrieve",
			"parameters": {
				"uri": "http:\/\/s3.amazonaws.com\/public.si2g\/input.zip",
				"audience": "https:\/\/ta-gamete-api-dev.azure-api.net",
				"callback": "http:\/\/127.0.0.1\/importer\/127",
				"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5rUkNSRFJFUlRZeE9FUTBNamN4T0RnNVEwTkJSVU5ETXpkR09UUTBOa1V3T1RCRU16ZEJRdyJ9.eyJodHRwczovL2dhbWV0ZS5jb20vYXBwX21ldGFkYXRhIjp7ImNvbXBhbnlJZCI6IjRjMzE5ZjM4LTYwYjYtNGMwMi04ZDRlLWExYjU3YWIzYjExMCIsInJvbGVzIjpbIkFkbWluIl0sInVzZXJJZCI6IjQ0YzhjNTQxLTYxYmMtNDE5Mi1hZGE2LTZmZjNmMzQ1YWFkYiIsImJyYW5kIjoidGllcnJhIn0sImh0dHBzOi8vdGEtZ2FtZXRlLWFwaS1kZXYuYXp1cmUtYXBpLm5ldC9hcHBfbWV0YWRhdGEiOnsiY29tcGFueUlkIjoiNGMzMTlmMzgtNjBiNi00YzAyLThkNGUtYTFiNTdhYjNiMTEwIiwicm9sZXMiOlsiQWRtaW4iXSwidXNlcklkIjoiNDRjOGM1NDEtNjFiYy00MTkyLWFkYTYtNmZmM2YzNDVhYWRiIiwiYnJhbmQiOiJ0aWVycmEifSwiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0L3VzZXJfbWV0YWRhdGEiOnsiZXhwaXJlcyI6IiIsInBob25lIjoiIiwic3VybmFtZSI6IlNwb3RsaWdodCIsIm5hbWUiOiJEZXZVc2VyIiwibmlja25hbWUiOiI0NGM4YzU0MS02MWJjLTQxOTItYWRhNi02ZmYzZjM0NWFhZGIiLCJsb2NhbGUiOiJlbiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfSwiaXNzIjoiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YTc0MzAzMDBhNmI0MTcxNWFlYTczODMiLCJhdWQiOlsiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0IiwiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MjgyOTc4OTgsImV4cCI6MTUyODM4NDI5OCwiYXpwIjoiQ2I1T0s1SUJzNG9QOVpDZXlXU0M0MGNEcjZpU3VnQWoiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCJ9.Ceocfk9j1BjcwQbaZVYSWoUdvqzDgPjQTqpKSPiJD1IPey9WFLwYTzK4OMpK8HJpRZ34-MtzHwnE2qT5DBV4cFrGu1nu7LA3QDjS2gvQbbMktS-JwRNwN3Y_Ce7paiR3e9aeNSmbEFdvx173OIGt-w5poHio2bweVTMFFMHcu64Q3JyjU55El_Nk1L2nevh2fW7w2_ByP_vBsHqFaWWJEYKIw9I4HtSBnGojMUXSwT2qptDe5fkPwXbhg13jGlxBeFWIS7bi4VcArbIao9N0RQVHYj-yMX7OYWtgPjbMXlOa1jOhZTm67Wd38NkMYtu_BbAXjD1bJbiIyg7cqTF1xg"
			},
			"inputs": [],
			"outputs": []
		}, {
			"block": "2",
			"step": "2",
			"task": "DataIngest",
			"parameters": {
				"sessionId": "127",
				"companyId": "4c319f38-60b6-4c02-8d4e-a1b57ab3b110",
				"userId": "44c8c541-61bc-4192-ada6-6ff3f345aadb",
				"callback": "http:\/\/127.0.0.1\/importer\/127",
				"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5rUkNSRFJFUlRZeE9FUTBNamN4T0RnNVEwTkJSVU5ETXpkR09UUTBOa1V3T1RCRU16ZEJRdyJ9.eyJodHRwczovL2dhbWV0ZS5jb20vYXBwX21ldGFkYXRhIjp7ImNvbXBhbnlJZCI6IjRjMzE5ZjM4LTYwYjYtNGMwMi04ZDRlLWExYjU3YWIzYjExMCIsInJvbGVzIjpbIkFkbWluIl0sInVzZXJJZCI6IjQ0YzhjNTQxLTYxYmMtNDE5Mi1hZGE2LTZmZjNmMzQ1YWFkYiIsImJyYW5kIjoidGllcnJhIn0sImh0dHBzOi8vdGEtZ2FtZXRlLWFwaS1kZXYuYXp1cmUtYXBpLm5ldC9hcHBfbWV0YWRhdGEiOnsiY29tcGFueUlkIjoiNGMzMTlmMzgtNjBiNi00YzAyLThkNGUtYTFiNTdhYjNiMTEwIiwicm9sZXMiOlsiQWRtaW4iXSwidXNlcklkIjoiNDRjOGM1NDEtNjFiYy00MTkyLWFkYTYtNmZmM2YzNDVhYWRiIiwiYnJhbmQiOiJ0aWVycmEifSwiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0L3VzZXJfbWV0YWRhdGEiOnsiZXhwaXJlcyI6IiIsInBob25lIjoiIiwic3VybmFtZSI6IlNwb3RsaWdodCIsIm5hbWUiOiJEZXZVc2VyIiwibmlja25hbWUiOiI0NGM4YzU0MS02MWJjLTQxOTItYWRhNi02ZmYzZjM0NWFhZGIiLCJsb2NhbGUiOiJlbiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfSwiaXNzIjoiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YTc0MzAzMDBhNmI0MTcxNWFlYTczODMiLCJhdWQiOlsiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0IiwiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MjgyOTc4OTgsImV4cCI6MTUyODM4NDI5OCwiYXpwIjoiQ2I1T0s1SUJzNG9QOVpDZXlXU0M0MGNEcjZpU3VnQWoiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCJ9.Ceocfk9j1BjcwQbaZVYSWoUdvqzDgPjQTqpKSPiJD1IPey9WFLwYTzK4OMpK8HJpRZ34-MtzHwnE2qT5DBV4cFrGu1nu7LA3QDjS2gvQbbMktS-JwRNwN3Y_Ce7paiR3e9aeNSmbEFdvx173OIGt-w5poHio2bweVTMFFMHcu64Q3JyjU55El_Nk1L2nevh2fW7w2_ByP_vBsHqFaWWJEYKIw9I4HtSBnGojMUXSwT2qptDe5fkPwXbhg13jGlxBeFWIS7bi4VcArbIao9N0RQVHYj-yMX7OYWtgPjbMXlOa1jOhZTm67Wd38NkMYtu_BbAXjD1bJbiIyg7cqTF1xg"
			},
			"inputs": [],
			"outputs": []
		}, {
			"block": "3",
			"step": "3",
			"task": "DataPublish",
			"parameters": {
				"publishUri": "http:\/\/127.0.0.1\/importer\/127\/callback",
				"callback": "http:\/\/127.0.0.1\/importer\/127",
				"authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5rUkNSRFJFUlRZeE9FUTBNamN4T0RnNVEwTkJSVU5ETXpkR09UUTBOa1V3T1RCRU16ZEJRdyJ9.eyJodHRwczovL2dhbWV0ZS5jb20vYXBwX21ldGFkYXRhIjp7ImNvbXBhbnlJZCI6IjRjMzE5ZjM4LTYwYjYtNGMwMi04ZDRlLWExYjU3YWIzYjExMCIsInJvbGVzIjpbIkFkbWluIl0sInVzZXJJZCI6IjQ0YzhjNTQxLTYxYmMtNDE5Mi1hZGE2LTZmZjNmMzQ1YWFkYiIsImJyYW5kIjoidGllcnJhIn0sImh0dHBzOi8vdGEtZ2FtZXRlLWFwaS1kZXYuYXp1cmUtYXBpLm5ldC9hcHBfbWV0YWRhdGEiOnsiY29tcGFueUlkIjoiNGMzMTlmMzgtNjBiNi00YzAyLThkNGUtYTFiNTdhYjNiMTEwIiwicm9sZXMiOlsiQWRtaW4iXSwidXNlcklkIjoiNDRjOGM1NDEtNjFiYy00MTkyLWFkYTYtNmZmM2YzNDVhYWRiIiwiYnJhbmQiOiJ0aWVycmEifSwiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0L3VzZXJfbWV0YWRhdGEiOnsiZXhwaXJlcyI6IiIsInBob25lIjoiIiwic3VybmFtZSI6IlNwb3RsaWdodCIsIm5hbWUiOiJEZXZVc2VyIiwibmlja25hbWUiOiI0NGM4YzU0MS02MWJjLTQxOTItYWRhNi02ZmYzZjM0NWFhZGIiLCJsb2NhbGUiOiJlbiIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlfSwiaXNzIjoiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YTc0MzAzMDBhNmI0MTcxNWFlYTczODMiLCJhdWQiOlsiaHR0cHM6Ly90YS1nYW1ldGUtYXBpLWRldi5henVyZS1hcGkubmV0IiwiaHR0cHM6Ly90YS1nYW1ldGUtZGV2LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MjgyOTc4OTgsImV4cCI6MTUyODM4NDI5OCwiYXpwIjoiQ2I1T0s1SUJzNG9QOVpDZXlXU0M0MGNEcjZpU3VnQWoiLCJzY29wZSI6Im9wZW5pZCBlbWFpbCJ9.Ceocfk9j1BjcwQbaZVYSWoUdvqzDgPjQTqpKSPiJD1IPey9WFLwYTzK4OMpK8HJpRZ34-MtzHwnE2qT5DBV4cFrGu1nu7LA3QDjS2gvQbbMktS-JwRNwN3Y_Ce7paiR3e9aeNSmbEFdvx173OIGt-w5poHio2bweVTMFFMHcu64Q3JyjU55El_Nk1L2nevh2fW7w2_ByP_vBsHqFaWWJEYKIw9I4HtSBnGojMUXSwT2qptDe5fkPwXbhg13jGlxBeFWIS7bi4VcArbIao9N0RQVHYj-yMX7OYWtgPjbMXlOa1jOhZTm67Wd38NkMYtu_BbAXjD1bJbiIyg7cqTF1xg"
			},
			"inputs": [],
			"outputs": []
		}
	],
	"auth": "f53147f6-08ae-469c-be9e-8b79055c0889"
}
WF_DEFINITION
, true);

        $this->assertEquals($expectedWfDefinition, $testWfDefinition);
    }
}
