<?php

    namespace WorkflowManager;
    
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
    
    class LogUtil {
        protected static $_defaultDirectory;
        protected static $_defaultFilename;
        protected static $_defaultLogLevel;
        protected $logger;
    
        function __construct($filename = null, $logLevel = null, $loggerName = "applicationLogger") {
            $this->logger = new Logger($loggerName);
            if ($filename == null) {
                $filename = self::$_defaultDirectory . self::$_defaultFilename . "-" . date("Y-m-d") . ".log";
            }
            if ($logLevel == null) {
                $logLevel = self::$_defaultLogLevel;
            }
            $this->logger->pushHandler(new StreamHandler($filename, $logLevel));
        }
    
        public function getLogger() {
            return $this->logger;
        }
    
        public static function _init($dir = "logs/", $filename = "output", $logLevel = Logger::INFO) {
            self::$_defaultDirectory = $dir;
            self::$_defaultFilename = $filename;
            self::$_defaultLogLevel = $logLevel;
        }
    }
    
    LogUtil::_init();
