# WorkflowManager

Il WorkflowManager � una libreria wrapper che permette di mandare comandi allo scheduler.

## Getting Started

Per poter funzionare ha bisogno di alcune configurazioni preliminari.

* **Env::$SAL_ENDPOINT**: endpoint da chiamare per invocare SAL. Generalmente IP locale e porta sono sufficienti.
* **Env::$WFS_IP**: indirizzo IP dove trovare lo scheduler.
* **Env::$WFS_ZEROMQ_ENDPOINT**: indirizzo IP e porta dell'endpoint ZeroMQ. � l'indirizzo dove il progetto connetter� ZeroMQ per 
comunicare con lo scheduler. Fornirlo nella forma completa con `tcp://`.
* **Env::$CTRL_IP**: indirizzo IP che viene utilizzato come _"sourceIp"_ per le richieste di traduzione SAL. Dovrebbe corrispondere
all'IP di questa macchina. L'indirizzo di loopback ha dimostrato di funzionare ugualmente.
* **Env::$WFS_ZEROMQ_SOURCE_ENDPOINT**: l'indirizzo IP e porta che andranno a riempire il campo _"source"_ delle richieste. Anche
questa stringa adr� in forma completa con `tcp://`.
* **Env::$WFS_NOTIFICATION_ENDPOINT**: indirizzo IP, eventuale porta e path in cui lo scheduler cercher� l'endpoint PUT `workflow`
per fornire gli aggiornamenti di status. Utilizzare le classi in "src/Workflow/Status/" per astrarre i JSON dalla richiesta dello scheduler
e della risposta a tale endpoint. 
* **Env::$WORKFLOW_BASE_FOLDER**: path assoluta alla cartella `data/workflow`. Viene utilizzata dal WorkflowBuilder per generare
la path dove immagazzinare i dati del workflow. Attualmente il suo effettivo utilizzo � in dubbio.
* **WFSchedulerHandler::$_sndTimeout**: (opzionale) timeout scheduler in send. Di default a 10000;
* **WFSchedulerHandler::$_rcvTimeout**: (opzionale) timeout scheduler in recieve. Di default a 10000;

### Prerequisites

Oltre alle dipendenze specificate in composer, il pacchetto richiede che ZeroMQ sia installato nel sistema ospitante e che il 
relativo modulo `mod_zmq` sia installato e disponibile in Apache.

### Installing

Installare Il modulo mod_zmq in Apache.

WorkflowManager viene fornito come pacchetto composer, quindi � sufficiente richiamare:

```
composer install
```

nella folder per installare il pacchetto insieme a tutte le sue dipendenze.

Segue un esempio di configurazione:

```
	require 'vendor/autoload.php';

	use WorkflowManager\Configuration\Env;
	use WorkflowManager\Messaging\WFSchedulerHandler;
    
	Env::$SAL_ENDPOINT = 'http://127.0.0.1:5504';
    
	Env::$WFS_IP = '127.0.0.1';
	Env::$WFS_ZEROMQ_ENDPOINT = 'tcp://127.0.0.1:5501';
    
	Env::$CTRL_IP = "127.0.0.1";
	Env::$WFS_ZEROMQ_SOURCE_ENDPOINT = 'tcp://127.0.0.1:*');
	Env::$WFS_NOTIFICATION_ENDPOINT = 'http://127.0.0.1/proj/root/';
	Env::$WORKFLOW_BASE_FOLDER = 'C:/Project/Data/workflow';
    
	// timeouts
	WFSchedulerHandler::$_sndTimeout = 10000;
	WFSchedulerHandler::$_rcvTimeout = 10000;
```